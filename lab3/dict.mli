(* lab3/dict.mli *)

type ident = string

type ptype = 
    Integer 
  | Boolean 
  | Array of int * ptype
  | Void

(* |def| -- definitions in environment *)
type def = 
  { d_tag: ident;               (* Name *)
    d_type: ptype;              (* Type *)
    d_lab: string }             (* Global label *)

type environment

(* |define| -- add a definition, raise Exit if already declared *)
val define : def -> environment -> environment

(* |lookup| -- search an environment or raise Not_found *)
val lookup : ident -> environment -> def

(* |init_env| -- initial empty environment *)
val init_env : environment

(* Function which tells us the size of a type *)
val type_size : ptype -> int

(* is this type an array *)
val is_array : ptype -> bool

(* base type of an array or exception *) 
val base_type : ptype -> ptype

(* bound of an array *)
val get_bound : ptype -> int
