(* lab3/dict.ml *)

(* Environments are implemented using a library module that 
   represents mappings by balanced binary trees. *)

type ident = string

type ptype = 
    Integer 
  | Boolean 
  | Array of int * ptype
  | Void

(* |def| -- definitions in environment *)
type def = 
  { d_tag: ident;               (* Name *)
    d_type: ptype;              (* Type *)
    d_lab: string }             (* Global label *)

module IdMap = Map.Make(struct type t = ident  let compare = compare end)

type environment = Env of def IdMap.t

let can f x = try f x; true with Not_found -> false

(* |define| -- add a definition *)
let define d (Env e) = 
  if can (IdMap.find d.d_tag) e then raise Exit;
  Env (IdMap.add d.d_tag d e)

(* |lookup| -- find definition of an identifier *)
let lookup x (Env e) = IdMap.find x e

(* |init_env| -- empty environment *)
let init_env = Env IdMap.empty

(* size of a type *)
let rec type_size =
  function 
      Boolean -> 1
    | Integer -> 4
    | Array (bound, p) -> bound * (type_size p)
    | Void -> 0

let is_array =
  function 
      Array(_,_) -> true
    | _ -> false

(* I might need to change failwith with some excetion stuff *)
let base_type =
  function
      Array(_, the_type) -> the_type
    | _ -> failwith "This is not an array" 

let get_bound =
  function
      Array(bound, _) -> bound
    | _ -> failwith "This is not an array" 
