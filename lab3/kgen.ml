(* lab3/kgen.ml *)

open Dict 
open Tree 
open Keiko 
open Print

(* |line_number| -- find line number of variable reference *)
let rec line_number e =
  match e.e_guts with
      Variable x -> x.x_line
    | Sub (a, e) -> line_number a
    | _ -> 999

(* |gen_expr| -- generate code for an expression *)
let rec gen_expr e =
  match e.e_guts with
      Variable _ | Sub _ ->
      if type_size e.e_type == 1 then SEQ[gen_addr e; LOADC]
      else SEQ[gen_addr e; LOADW]
    | Number n ->
        CONST n
    | Monop (w, e1) ->
        SEQ [gen_expr e1; MONOP w]
    | Binop (w, e1, e2) ->
        SEQ [gen_expr e1; gen_expr e2; BINOP w]

(* |gen_addr| -- generate code to push address of a variable *)
and gen_addr v =
  match v.e_guts with
      Variable x ->
        let d = get_def x in
        SEQ [LINE x.x_line; GLOBAL d.d_lab]
    | Sub (a, index) ->
            let bound = get_bound a.e_type in
            SEQ[gen_addr a; gen_index index (type_size v.e_type) bound; 
            CONST (type_size a.e_type); BOUND (line_number a); BINOP PlusA]
    | _ ->
        failwith "gen_addr"

and gen_index index ts bound =
    match index.e_guts with
    | Number x -> 
            if x < bound then
                SEQ[CONST (x * ts)]
            else failwith "Out of bounds..."
    | _ -> if ts == 1 then (gen_expr index) (* eliminate some extra calc *)
           else SEQ[gen_expr index; CONST ts; BINOP Times;]


(* |negate| -- map comparison op to its negation *)
let negate = 
  function Eq -> Neq | Neq -> Eq | Lt  -> Geq
    | Leq -> Gt | Gt  -> Leq | Geq -> Lt
    | _ -> failwith "negate"

(* |gen_cond| -- generate code for short-circuit condition *)
let rec gen_cond sense lab e =
  match e.e_guts with
      Number x ->
        let b = (x <> 0) in
        if b = sense then JUMP lab else NOP
    | Binop ((Eq|Neq|Lt|Gt|Leq|Geq) as w, e1, e2) ->
        SEQ [gen_expr e1; gen_expr e2;
          JUMPC ((if sense then w else negate w), lab)]
    | Monop (Not, e) ->
        gen_cond (not sense) lab e
    | Binop (And, e1, e2) ->
        if sense then begin
          let lab1 = label () in
          SEQ [gen_cond false lab1 e1;
            gen_cond true lab e2; LABEL lab1]
        end
        else begin
          SEQ [gen_cond false lab e1;
            gen_cond false lab e2]
        end
    | Binop (Or, e1, e2) ->
        if sense then begin
          SEQ [gen_cond true lab e1;
            gen_cond true lab e2]
        end
        else begin
          let lab1 = label () in
          SEQ [gen_cond true lab1 e1;
            gen_cond false lab e2; LABEL lab1]
        end
    | _ ->
        SEQ [gen_expr e; JUMPB (sense, lab)]

(* |gen_stmt| -- generate code for a statement *)
let rec gen_stmt =
  function
      Skip -> NOP
    | Seq ss -> SEQ (List.map gen_stmt ss)
    | Assign (v, e) ->
      if type_size v.e_type == 1 then 
        SEQ [LINE (line_number v); gen_expr e; gen_addr v; STOREC]
      else 
        SEQ [LINE (line_number v); gen_expr e; gen_addr v; STOREW]
    | Print e ->
        SEQ [gen_expr e; CONST 0; GLOBAL "Lib.Print"; PCALL 1]
    | Newline ->
        SEQ [CONST 0; GLOBAL "Lib.Newline"; PCALL 0]
    | IfStmt (test, thenpt, elsept) ->
        let lab1 = label () and lab2 = label () in
        SEQ [gen_cond false lab1 test;
          gen_stmt thenpt; JUMP lab2;
          LABEL lab1; gen_stmt elsept; LABEL lab2]
    | WhileStmt (test, body) ->
        let lab1 = label () and lab2 = label () in
        SEQ [LABEL lab1; gen_cond false lab2 test;
          gen_stmt body; JUMP lab1; LABEL lab2]

let gen_decl (Decl (xs, t)) =
  List.iter (fun x ->
      let d = get_def x in
      (* now calculates s by type_size *)
      let s = type_size t in
      printf "GLOVAR $ $\n" [fStr d.d_lab; fNum s]) xs

(* |translate| -- generate code for the whole program *)
let translate (Program (ds, ss)) = 
  printf "PROC MAIN 0 0 0\n" [];
  Keiko.output (gen_stmt ss);
  printf "RETURN\n" [];
  printf "END\n\n" [];
  List.iter gen_decl ds



