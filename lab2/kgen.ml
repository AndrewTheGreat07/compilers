(* lab2/kgen.ml *)

open Tree
open Keiko

(* |gen_expr| -- generate code for an expression *)
let rec gen_expr =
  function
      Variable x ->
        SEQ [LINE x.x_line; LDGW x.x_lab]
    | Number x ->
        CONST x
    | Monop (w, e1) ->
        SEQ [gen_expr e1; MONOP w]
    | Binop (w, e1, e2) ->
        SEQ [gen_expr e1; gen_expr e2; BINOP w]

(* |negate| -- map comparison operator to its opposite *)
let negate = 
  function Eq -> Neq | Neq -> Eq | Lt  -> Geq
    | Leq -> Gt | Gt -> Leq | Geq -> Lt
    | _ -> failwith "negate"

(* |gen_cond| -- generate code for short-circuit condition *)
let rec gen_cond sense lab e =
  (* Generate code to jump to |lab| if |e| has value |sense| *)
  match e with
      Number x ->
        let b = (x <> 0) in
        if b = sense then JUMP lab else NOP
    | Binop ((Eq|Neq|Lt|Gt|Leq|Geq) as w, e1, e2) ->
        SEQ [gen_expr e1; gen_expr e2;
          JUMPC ((if sense then w else negate w), lab)]
    | Monop (Not, e) ->
        gen_cond (not sense) lab e
    | Binop (And, e1, e2) ->
        if sense then begin
          let lab1 = label () in
          SEQ [gen_cond false lab1 e1; 
            gen_cond true lab e2; LABEL lab1]
        end
        else begin
          SEQ [gen_cond false lab e1;
            gen_cond false lab e2]
        end
    | Binop (Or, e1, e2) ->
        if sense then begin
          SEQ [gen_cond true lab e1;
            gen_cond true lab e2]
        end
        else begin
          let lab1 = label () in
          SEQ [gen_cond true lab1 e1;
            gen_cond false lab e2; LABEL lab1]
        end
    | e ->
        SEQ [gen_expr e; JUMPB (sense, lab)]


let arm (x, y) = CASEARM (x, y)
(* adds the labs to each element of numbers creating a list of pairs 
 * Somethiing like: List.map pair_up numbers (List.repeat labs) *)
let rec someFunc (numbers, labs) =
    match numbers with
      [] -> []
    | x :: xs -> (x, labs) :: (someFunc(xs, labs))

(* |gen_stmt| -- generate code for a statement *)
let rec gen_stmt thisStmt scopeLabel =
(*  function *)
    match thisStmt with
      Skip -> NOP
    | Seq stmts -> let f s = gen_stmt s scopeLabel in
        SEQ (List.map f stmts)
    | Assign (v, e) ->
        SEQ [LINE v.x_line; gen_expr e; STGW v.x_lab]
    | Print e ->
        SEQ [gen_expr e; CONST 0; GLOBAL "Lib.Print"; PCALL 1]
    | Newline ->
        SEQ [CONST 0; GLOBAL "Lib.Newline"; PCALL 0]
    | IfStmt (test, thenpt, elsept) ->
        let lab1 = label () and lab2 = label () in
        SEQ [gen_cond false lab1 test;
          gen_stmt thenpt scopeLabel; JUMP lab2;
          LABEL lab1; gen_stmt elsept scopeLabel; LABEL lab2]
    | WhileStmt (test, body) ->
        let lab1 = label () and lab2 = label () in
        SEQ [LABEL lab1; gen_cond false lab2 test;
          gen_stmt body scopeLabel; JUMP lab1; LABEL lab2]
    (* because of the exit statement we need another label for the end of the
     * repeat loop *)
    | RepeatStmt (body, test) ->
        let lab = label () in
        SEQ [LABEL lab; gen_stmt body scopeLabel; gen_cond false lab test]
    | LoopStmt body -> 
        let initLab = label() and exitLab = label() in
        SEQ [LABEL initLab; gen_stmt body exitLab; JUMP initLab; LABEL exitLab]
    | ExitStmt -> SEQ[JUMP scopeLabel]
    | CaseStmt (switch, cases, default) ->
        let labels = List.map (fun x -> label()) cases and defaultLab = label() and exitLab = label () in
        (* table is a list of case numbers and labels *)
        (* List.combine puts the labels with every cases block
         * and List.map spreads the labels around for each case *)
        let table = List.concat( List.map someFunc (List.combine (List.map fst cases) labels) ) in
        (* table 2 - creates a list o sequences for the case block*)
        let table2 = List.concat (List.map (gen_seq scopeLabel exitLab) (List.combine labels cases))
        and n = List.length(table) in
        SEQ([gen_expr switch; CASEJUMP n] @ (List.map arm table) @ 
        [JUMP defaultLab;] @ table2 @ [LABEL defaultLab; gen_stmt default scopeLabel; LABEL exitLab]) 
    and gen_seq currentLab ex (lab, (numbers, st)) = [LABEL lab; gen_stmt st currentLab; JUMP ex;]

(* |translate| -- generate code for the whole program *)
let translate (Program ss) =
    let startLabel = label () in
    Keiko.output (gen_stmt ss startLabel)
